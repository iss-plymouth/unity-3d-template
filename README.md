README
=========

---

Personal Details
===
- ** Student Name **: <<FIRST NAME & LAST NAME>>
- ** Student Number **: <<STUDENT NUMBER>>
- ** Course Title **: <<COURSE TITLE>>
- ** Module Code **: <<MODULE CODE>>

----

Project Details
===
<<ENTER PROJECT DETAILS HERE>>

----

Credits
===
- <<ENTER ASSETS USED>>
- <<PEOPLE TO CREDIT>>